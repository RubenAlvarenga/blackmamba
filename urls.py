"""blackmamba URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
from rest_framework_swagger.views import get_swagger_view
from socio.api import custom404
from django.conf import settings
from django.conf.urls.static import static


schema_view = get_swagger_view(title='BLACKMAMBA API')
handler404 = custom404



urlpatterns = [
    url(r'^docs/$', schema_view),
    url(r'^admin/', admin.site.urls),

    url(r'^ahorro/', include('ahorro.urls', namespace="ahorro", app_name='ahorro')),
]  + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
