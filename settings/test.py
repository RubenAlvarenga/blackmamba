from .base import *
import os

DEBUG = True

ALLOWED_HOSTS = ['*']

WSGI_APPLICATION = 'blackmamba.wsgi.test.application'

MEDIA_ROOT = BASE_DIR.child('media')
STATICFILES_DIRS = [os.path.join(BASE_DIR, 'static')]
#STATIC_ROOT = os.path.join(BASE_DIR, 'static')
STATIC_URL = '/static/'
MEDIA_URL = 'http://35.198.14.144:8000/media/'

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': 'blackmamba',
        'USER': 'postgres',
        'PASSWORD': 'postgres',
        'HOST': '138.186.62.6',
        'PORT': '5432',
    }
}
